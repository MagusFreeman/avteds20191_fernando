function reqServlet(json, url, callback) {

	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			callback(this.responseText);
		}
	};

	xhttp.open("POST", url, true);
	xhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');

	if (json != null) {
		xhttp.send(JSON.stringify(json));
	} else {
		xhttp.send();
	}

}

var Cliente = {
	addCliente : function(e) {
		e.preventDefault();

		var t = {};
		var endereco = {};
		t.nome = document.getElementById("nome").value;
		t.cpf = document.getElementById("cpf").value;
		endereco.logradouro = document.getElementById("logradouro").value;
		endereco.numero = document.getElementById("numero").value;
		endereco.cidade = document.getElementById("cidade").value;
		endereco.estado = document.getElementById("estado").value;
		endereco.cep = document.getElementById("cep").value;
		t.endereco = endereco;

		reqServlet(t, "addcliente", Cliente.callbackAddCliente);

		return false;
	},

	callbackAddCliente : function(responseText) {
		var resp = JSON.parse(responseText);
		var msg = document.getElementById("msg");
		msg.style.display = "block";
		msg.innerHTML = resp.msg;
	},
	
	retornaCliente : function() {		

		reqServlet(null, "loadcliente", Cliente.callbackRetornaCliente);
	},

	callbackRetornaCliente : function(responseText) {
		var resp = JSON.parse(responseText);
		var tbody = document.querySelector("tbody");
		tbody.innerHTML = "";
		for(var i=0; i<resp.data.length; i++){
			var tr = document.createElement("tr");		
			var nome = document.createElement("td");
			nome.textContent = resp.data[i].nome;
			var cpf = document.createElement("td");
			cpf.textContent = resp.data[i].cpf;
			var logradouro = document.createElement("td");
			logradouro.textContent = resp.data[i].endereco.logradouro;
			var numero = document.createElement("td");
			numero.textContent = resp.data[i].endereco.numero;
			var cidade = document.createElement("td");
			cidade.textContent = resp.data[i].endereco.cidade;
			var estado = document.createElement("td");
			estado.textContent = resp.data[i].endereco.estado;
			var cep = document.createElement("td");
			cep.textContent = resp.data[i].endereco.cep;
			
			tr.appendChild(nome);
			tr.appendChild(cpf);
			tr.appendChild(logradouro);
			tr.appendChild(numero);
			tr.appendChild(cidade);
			tr.appendChild(estado);
			tr.appendChild(cep);
			tbody.appendChild(tr);
		}
		var visTable = document.querySelector("table");
		visTable.style.display = "inline";

	}
	
}




/*
function loadDoc() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("demo").innerHTML = this.responseText;
		}
	};
	xhttp.open("GET", "teste.txt", true);
	xhttp.send();
}
*/
